﻿using NASARover.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace NASARover.Helpers
{
    class RoverHelper
    {
        //Check if the Rover is in the Plateu Grid
        static public bool CheckRoverInGrid(Rover rover, TestData RoverData)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            //Check if the Rover is inside the Grid
            switch (rover.CurrentPosition.Direction)
            {
                case "N":
                    if (rover.CurrentPosition.Y == RoverData.GridSize.Y)
                    {
                        Console.WriteLine("{0} : Unable to move North, Will fall off the egde, I'm not moving", rover.RoverName);
                        return false;
                    }
                    break;
                case "S":
                    if (rover.CurrentPosition.Y == 0)
                    {
                        Console.WriteLine("{0} : Unable to move South, Will fall off the egde, I'm not moving", rover.RoverName);
                        return false;
                    }
                    break;
                case "E":
                    if (rover.CurrentPosition.X == RoverData.GridSize.X)
                    {
                        Console.WriteLine("{0} : Unable to move East, Will fall off the egde, I'm not moving", rover.RoverName);
                        return false;
                    }
                    break;
                case "W":
                    if (rover.CurrentPosition.X == 0)
                    {
                        Console.WriteLine("{0} : Unable to move East, Will fall off the egde, I'm not moving", rover.RoverName);
                        return false;
                    }
                    break;
            }
            //Return true if Rover in the grid
            return true;
        }

        //Check if the Rover doesn't move into another rover
        static public bool CheckIfAnyRoverObstructing(Rover rover, TestData RoverData)
        {
            //Set the future stage of the rover
            var newX = rover.CurrentPosition.X;
            var newY = rover.CurrentPosition.Y;
            switch (rover.CurrentPosition.Direction)
            {
                case "N": newY += 1; break;
                case "S": newY -= 1; break;
                case "E": newX += 1; break;
                case "W": newX -= 1; break;
                default: Console.ForegroundColor = ConsoleColor.Red; Console.WriteLine("Invalid Direction"); break;
            }

            //Search the List of Rovers to check if any rover is currently in the way
            int index = RoverData.Rovers.FindIndex(item => item.CurrentPosition.X == newX && item.CurrentPosition.Y == newY);
            if (index >= 0)
            {
                //Return false and Message
                Console.WriteLine("{0} : Unable to move will drive into another rover at ({1},{2})", rover.RoverName, newX, newY);
                return false;
            }
            //No Rover in the way
            return true;
        }

        //Instruction for the Rover
        static public void Action(Rover rover, char Command, TestData RoverData)
        {
            //Check the Action
            switch (Command)
            {
                //Call the Turn Left Method
                case 'L': TurnLeft(rover); break;
                //Call the Turn Right Method
                case 'R': TurnRight(rover); break;
                //Call the Move Method
                case 'M': Move(rover, RoverData); break;
                //Error out if Unknown Command
                default: Console.ForegroundColor = ConsoleColor.Red; Console.WriteLine("Invalid Command"); break;
            }
        }

        //Method to Move the rover in the direction it's facing
        static public void Move(Rover rover, TestData RoverData)
        {
            //Check Rover is in the Griud
            if (CheckRoverInGrid(rover, RoverData))
            {
                //Check Rover if there are any Obstructs
                if (CheckIfAnyRoverObstructing(rover, RoverData))
                {
                    //Move the Rover base on Direction
                    switch (rover.CurrentPosition.Direction)
                    {
                        case "N": rover.CurrentPosition.Y += 1; break;
                        case "S": rover.CurrentPosition.Y -= 1; break;
                        case "E": rover.CurrentPosition.X += 1; break;
                        case "W": rover.CurrentPosition.X -= 1; break;
                        default: Console.ForegroundColor = ConsoleColor.Red; Console.WriteLine("Invalid Direction"); break;
                    }
                }
            }
        }

        //Method for Turning the Rover Right
        static public void TurnRight(Rover rover)
        {
            //Move the direction to the right based on current direction
            switch (rover.CurrentPosition.Direction)
            {
                case "N": rover.CurrentPosition.Direction = "E"; break;
                case "S": rover.CurrentPosition.Direction = "W"; break;
                case "E": rover.CurrentPosition.Direction = "S"; break;
                case "W": rover.CurrentPosition.Direction = "N"; break;
                default: Console.ForegroundColor = ConsoleColor.Red; Console.WriteLine("Invalid Direction"); break;
            }
        }

        //Method for Turning the Rover Left
        static public void TurnLeft(Rover rover)
        {
            //Move the direction to the left based on current direction
            switch (rover.CurrentPosition.Direction)
            {
                case "N": rover.CurrentPosition.Direction = "W"; break;
                case "S": rover.CurrentPosition.Direction = "E"; break;
                case "E": rover.CurrentPosition.Direction = "N"; break;
                case "W": rover.CurrentPosition.Direction = "S"; break;
                default: Console.ForegroundColor = ConsoleColor.Red; Console.WriteLine("Invalid Direction"); break;
            }
        }

        //Get the Test Data from Json
        static public TestData GetTestData()
        {
            try
            {
                //Get the Test Data 
                using (StreamReader r = new StreamReader("./TestData.json"))
                {
                    //Deserilalize json to Items Object and return the object 
                    return JsonConvert.DeserializeObject<TestData>(r.ReadToEnd());
                }
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("There was issue reading the Data and Converting to an Object /r" + ex.Message);
                return null;
            }
        }
    }
}
