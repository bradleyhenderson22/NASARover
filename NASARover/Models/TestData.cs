﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NASARover.Models
{
    public class TestData
    {
        public GridSize GridSize { get; set; }
        public List<Rover> Rovers { get; set; }
    }

    public class GridSize
    {
        public int X { get; set; }
        public int Y { get; set; }
    }

    public class Rover
    {
        public Guid UUID { get; set; }
        public String RoverName { get; set; }
        public Coordinates CurrentPosition { get; set; }
        public string Instructions { get; set; }
    }

    public class Coordinates
    {
        public int X { get; set; }
        public int Y { get; set; }
        public String Direction { get; set; }
    }
}

