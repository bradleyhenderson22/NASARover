﻿using NASARover.Helpers;
using NASARover.Models;
using System;

namespace NASARover
{
    //Inheritance the Rover Helper Class
    class Program : RoverHelper
    {
        //Main Method
        static void Main(string[] args)
        {
            //Load Test Data
            TestData Items = GetTestData();
            //Loop though each rover
            foreach (Rover rover in Items.Rovers)
            {
                //Loop thought each character in the insturctions
                foreach (char Command in rover.Instructions)
                {
                    //Excute Command on the Rover
                    Action(rover, Command, Items);
                }
                //Print out the rover new location
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("{0} : Postion ({1},{2}) {3}", rover.RoverName, rover.CurrentPosition.X, rover.CurrentPosition.Y, rover.CurrentPosition.Direction);
            }
            //Don't close App(Debugging)
            Console.ReadLine();
        }
    }
}
